from django.db import models
from django.conf import settings

AUTH_USER_MODEL = settings.AUTH_USER_MODEL

# Create your models here.


class MealPlan(models.Model):
    name = models.CharField(max_length=120)
    date = models.DateTimeField(auto_now_add=False)
    owner = models.ForeignKey(AUTH_USER_MODEL, on_delete=models.CASCADE)
    recipes = models.ManyToManyField("recipes.Recipe", related_name="mealplans")

    def __str__(self):
        return self.name
